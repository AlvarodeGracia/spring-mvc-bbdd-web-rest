<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
    <head>
    <style>
    	
    		.error {
    			color: red;
    		}
    	
    	</style>
    </head>
    <body>
        <h3>Bienvenido, introduzca los detalles del Profesor</h3>
        
		<form:form method="POST"
          action="addProfesor" modelAttribute="profesorForm">
             <table>
                <tr>
                    <td><form:label path="nombre">Name</form:label></td>
                    <td><form:input path="nombre"/></td>
                    <td><form:errors path="nombre" cssClass="error"></form:errors></td>
                </tr>
                <tr>
                    <td><form:label path="id">Id</form:label></td>
                    <td><form:input path="id"/></td>
                    <td><form:errors path="id" cssClass="error"></form:errors></td>
                </tr>
                <tr>
                    <td><form:label path="email">
                     Email</form:label></td>
                    <td><form:input path="email"/></td>
                    <td><form:errors path="email" cssClass="error"></form:errors></td>
                </tr>
                 <tr>
                    <td><form:label path="seminario">
                     Seminario</form:label></td>
                    <td><form:input path="seminario"/></td>
                    <td><form:errors path="seminario" cssClass="error"></form:errors></td>
                </tr>
                <tr>
                    <td><input type="submit" value="Submit"/></td>
                </tr>
            </table>
        </form:form>
        
    </body>
</html>