package com.alvaro.spring.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

//Esto le indica al programa que es un bean y que es un controlador
@Controller
public class HelloController {

	//@GetMapping
	//@PostMapping
	//@PutMapping
	//@DeleteMapping
	//@PatchMapping
	//Por defecto get 
    @RequestMapping("/hello")
    public String sayHello(Model model) {        
        model.addAttribute("saludo", "Hola Mundo!!!");
        model.addAttribute("mensaje","Me llena de orgullo y satisfacción saludaros en este primer ejemplo de Spring MVC");
        model.addAttribute("url", "http://www.openwebinars.net");
        return "hello";
    }    
}
