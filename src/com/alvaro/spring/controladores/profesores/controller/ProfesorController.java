package com.alvaro.spring.controladores.profesores.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alvaro.spring.controladores.profesores.model.Profesor;
import com.alvaro.spring.controladores.profesores.services.ProfesorService;
import javax.validation.Valid;

@Controller
@RequestMapping("/profesores")
public class ProfesorController {
	
	@Autowired
	private ProfesorService profesorService;
	
	//@RequestMapping(value="/list", method= RequestMethod.GET)
	@GetMapping(value="/list") 
	public String listado(Model model) {
		model.addAttribute("profesores", profesorService.getProfesores());
		return "list";
		
	}
	
	
	@RequestMapping(value="/detail/{id_profesor}", method = RequestMethod.GET)
	//con @PathVariable caputaramos una variable de la url que no se llame igual que la entrada de la funcion 
	//en este caso id_profesor != id
	public String detalle(@PathVariable("id_profesor") int id, Model model) {
		
		model.addAttribute("profesor", profesorService.getById(id));
		
		return "details";
		
	}
	@RequestMapping(value="/redirect")
	public String reedireccion() {
		
		return "redirect:list";
	}
	
	@RequestMapping(value="/detail/{idProfesor}/asig/{posAsig}", method = RequestMethod.GET)
	public String asignatura(@PathVariable("idProfesor") int idProf, @PathVariable("posAsig") int posAsig, Model model) {
		
		model.addAttribute("asignatura", profesorService.getById(idProf).getAsignaturas().get(posAsig));
		
		return "asig";
	}
	
	
	@RequestMapping(value="/registrar")
	public String registrarProfesor(Model model) {
		
		Profesor profesor = new Profesor();
		
		model.addAttribute("profesorForm", profesor);
		
		return "form";
		
	}
	
	//@valid no aseguramos de que el objeto que viene sea valido
	@PostMapping(value="/addProfesor")
	public String addProfesor(@Valid @ModelAttribute("profesorForm") Profesor profesor, Model model) {
		
		profesorService.addProfesor(profesor);
		System.out.println(profesor.toString());
		return "redirect:list";
		
	} 

}
