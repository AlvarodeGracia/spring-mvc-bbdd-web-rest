package com.alvaro.spring.controladores.profesores.services;

import java.util.List;
import java.util.Optional;

import com.alvaro.spring.controladores.profesores.model.Profesor;

public interface ProfesorService {
	
	public List<Profesor> getProfesores();
	
	public Profesor getById(int id);
	
	public void addProfesor(Profesor profesor);

}
