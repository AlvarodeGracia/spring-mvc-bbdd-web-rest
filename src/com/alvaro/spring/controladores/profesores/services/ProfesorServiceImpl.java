package com.alvaro.spring.controladores.profesores.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alvaro.spring.controladores.profesores.data.ProfesorRepository;
import com.alvaro.spring.controladores.profesores.data.ProfesorRepositoryImpl;
import com.alvaro.spring.controladores.profesores.model.Profesor;

@Service
public class ProfesorServiceImpl implements ProfesorService {
	
	@Autowired
	private ProfesorRepository profesorRepository;

	@Override
	public List<Profesor> getProfesores() {
		// TODO Auto-generated method stub
		return profesorRepository.findAll();
	}

	@Override
	public Profesor getById(int id) {
		// TODO Auto-generated method stub
		return profesorRepository.findOne(id);
	}

	@Override
	public void addProfesor(Profesor profesor) {
		
		profesorRepository.save(profesor);
	}
	
	

}
