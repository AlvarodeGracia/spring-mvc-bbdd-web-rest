package com.alvaro.spring.controladores.profesores.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alvaro.spring.controladores.profesores.model.Profesor;

//Hay cvarias interfaces de las que crear nuestra interfaz
//Repository
//Crud Repository -> Repository
//PagingAndSotingRepository -> Crud Repository
//JpaRepository -> PagingAndSotingRepository
//Como vemos la ultima hereda de todas y nos ofrece mayor funcionalidad

//Clase con el que va atrabajar y tip ode ID
@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, Integer> {

}
