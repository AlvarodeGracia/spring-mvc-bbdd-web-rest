package com.alvaro.spring.controladores.profesores.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.alvaro.spring.controladores.profesores.model.Asignatura;
import com.alvaro.spring.controladores.profesores.model.Profesor;

@Repository
public class ProfesorRepositoryImpl {
	
	private Map<Integer, Profesor> profesores;
	
	public ProfesorRepositoryImpl() {
		profesores = new HashMap<Integer, Profesor>();
		
		List<Asignatura> asignaturas = new ArrayList<Asignatura>();
		asignaturas.add(new Asignatura(1,"Programacion", 10));
		
		profesores.put(1, new Profesor(1, "Carlos", "Sistemas", "carlos@openwbn.net",asignaturas));
		profesores.put(2, new Profesor(2, "Alberto", "Linux", "alberto@openwbn.net",asignaturas));
		profesores.put(3, new Profesor(3, "Luis Miguel", "Java", "luismi@openwbn.net",asignaturas));
		profesores.put(4, new Profesor(4, "Sim�n", ".Net", "simon@openwbn.net",asignaturas));
		
	}
	
	public Profesor getById(int id) {
		return profesores.get(id);
	}
	
	public List<Profesor> getProfesores(){
		List<Profesor> list = new ArrayList<Profesor>(profesores.values());
		return list;
	}
	
	public void addProfesor(Profesor profesor) {
		
		profesores.put(profesor.getId(), profesor);
	}

}
