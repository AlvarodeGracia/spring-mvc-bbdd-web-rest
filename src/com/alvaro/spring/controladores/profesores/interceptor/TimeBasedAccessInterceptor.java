package com.alvaro.spring.controladores.profesores.interceptor;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class TimeBasedAccessInterceptor extends HandlerInterceptorAdapter{
	
	private int openingTime;
	private int closingTime;
	
	public int getOpeningTime() {
		return openingTime;
	}
	public void setOpeningTime(int openingTime) {
		this.openingTime = openingTime;
	}
	public int getClosingTime() {
		return closingTime;
	}
	public void setClosingTime(int closingTime) {
		this.closingTime = closingTime;
	}
	
	//Antes de ejecutar el controlador
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		System.out.println("Ejecutando interfecpot pre");
		
		if(!request.getServletPath().contentEquals("/closed")) {
			System.out.println("No es closed");
			Calendar cal = Calendar.getInstance();
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			System.out.println("Hora actual: "+hour);
			if(openingTime <= hour && hour < closingTime) {
				System.out.println("Esta dentro de la hora de uso que es de "+openingTime+" a "+closingTime);
				return true;
			}
			
			response.sendRedirect(request.getContextPath()+ "/closed");
			return false;
			
		}else {
			return true;
		}
			
	}
	//Despues de ejecutar el manejador
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}
	
	//Despues de enviar el Request
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}
	
	

}
