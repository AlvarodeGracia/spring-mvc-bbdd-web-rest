package com.alvaro.spring.controladores.profesores.model;

public class Asignatura {
	
	private int id;
	private String titulo;
	private int numHoras;
	
	
	public Asignatura() {
		super();
		this.id = 0;
		this.titulo = "";
		this.numHoras = 0;
	}
	
	
	
	public Asignatura(int id, String titulo, int numHoras) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.numHoras = numHoras;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public int getNumHoras() {
		return numHoras;
	}



	public void setNumHoras(int numHoras) {
		this.numHoras = numHoras;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asignatura other = (Asignatura) obj;
		if (id != other.id)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Asignatura [id=" + id + ", titulo=" + titulo + ", numHoras=" + numHoras + "]";
	}
	
	
	
	

}
