package com.alvaro.spring.controladores.profesores.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="PROFESOR")
public class Profesor {
	
	//@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	//@Column(name="id")
	@NotNull
	private int id;
	
	//@Column(name="nombre")
	@NotEmpty
	@Size(min=2, max = 30)
	private String nombre;
	
	//@Column(name="seminario")
	private String seminario;
	
	//@Column(name="email")
	@NotEmpty
	@Email
	private String email;
	private List<Asignatura> asignaturas;
	
	
	public Profesor() {
		super();
		this.id = 0;
		this.nombre = "";
		this.seminario = "";
		this.email = "";
		List<Asignatura> asignaturas = null;
	}
	
	
	public Profesor(int id, String nombre, String seminario, String email, List<Asignatura> asignaturas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.seminario = seminario;
		this.email = email;
		this.asignaturas = asignaturas;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getSeminario() {
		return seminario;
	}


	public void setSeminario(String seminario) {
		this.seminario = seminario;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public List<Asignatura> getAsignaturas() {
		return asignaturas;
	}


	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profesor other = (Profesor) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Profesor [id=" + id + ", nombre=" + nombre + ", seminario=" + seminario + ", email=" + email + "]";
	}
	
	

}
